# Application
## Exporter l'application

Voici l'explication pour exporter l'application en fichier apk ou aab

### Réglages android

**A activer**

- Use Custom Build
- Godot Tts
- Armeabi-v7a
- Arm 64-v 8a
- Signed
- Classify As Game
- 32 Bits Framebuffer
- Immersive Mode
- Support Small
- Support Normal
- Support Large
- Support Xlarge
- Internet
- Read External Storage
- Write External Storage

### Keystore

Dans la section **Keystore** ajouter dans *Release* le chemin vers le fichier artiphonie.keystore
Mettre dans *Rekease User* : artiphonie
Mettre dans *Release Password* : artiphonie

### Ressources

Dans la section **Ressources** 

- Mode d'exportation : Exporter toutes les ressources du projet
- Filtres pour exporter des fichiers non-ressources : data/*,assets/*,*.json,art/*,*.png

### Version

Si vous souhaitez créer une nouvelle version à publier sur le play store, il faut :

- Mettre une nouvelle version dans la section version
- Exporter en aab sans mode debug


## Obtenir les logs de l'application sur un appareil

Si vous avez des erreurs sur les appareils qui n'apparaissent pas dans l'éditeur et que vous souhaitez voir les logs.

Pré-requis : Avoir Android Studio installé

1. Activez le mode développeur de l'appareil
2. Activez le débugage USB
3. Branchez l'appareil à votre ordinateur
4. Lancer la commande "adb devices". Vous devez avoir une liste avec un appareil (suite de symbole) et "device" marqué sur la ligne. Si vous n'avez pas "device", c'est que vous n'avez pas accepté que l'ordinateur accede à l'appareil
5. Lancer ensuite "adb logcat -s godot" pour avoir uniquement les logs de godot