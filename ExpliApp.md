# Explication installation application Artiphonie

## 1. **Installation via APK**

Vous pouvez télécharger le fichier d'installation de l'application sur la page d'accueil d'Artiphonie et en cliquant sur le lien *Télécharger l'application (apk)*

Une fois l'application télécharger. Cliquez sur le fichier téléchargé dans votre barre de notification ou dans *Fichiers* puis *Téléchargement*, puis installez en suivant les instructions.

Vous pouvez ensuite utiliser l'application.

## 2. **Aide**

Pour obtenir de l'aide, faire des remarques ou avoir plus de renseignement n'hésitez pas à compter etienne.reygner@etu.univ-grenoble-alpes.fr