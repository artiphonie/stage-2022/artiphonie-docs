# Documentation Orthophoniste

Documentation pour prendre en main le panel d'administration d'Artiphonie.

## 1. **Accéder au panel d'administration**

Pour accéder au panel d'Artiphonie et pouvoir utiliser les fonctionnalités vous devez posséder un compte.

**Il n'est pas possible de créer un compte par vous-même. Vous devez demander à une personne possédant les droits de création de vous créer compte.**

### 1.1 **Connexion au compte**

Lors de votre première visite sur le panel d'Artiphonie vous êtes redirigés vers la page de connexion. Vous pouvez entrer les identifiants qui vous ont été confiés.

Après la validation de vos identifiants de connexion il vous sera demandé pour votre première visite de modifier votre mot de passe.
Mettez à jour votre mot de passe puis vous serez automatiquement redirigés vers le panel d'administration.


## 2. **Utiliser le panel**

Une fois connecté vous avez accès et vous pouvez utiliser le panel des fonctionnalités d'Artiphonie.

![Image panel](IMG_PANEL/panel.png)

Image exemple du panel

### *Explications*

Vous avez sur le panel deux sections, patients et dictionnaire

### 2.1 **Patients**

Vous avez accès à tous vos patients. Vous pouvez également créer un nouveau patient ou voir les informations liés à un patient.

#### 2.1.1 **Créer un compte patient**

Cliquez sur le bouton *Créer un compte patient*.

Un formulaire vous demande de remplir les informations lié à ce nouveau patient, puis de cliquez sur *Créer*

![Créer compte patient](IMG_PANEL/creer_compte_patient.png)

Image exemple de la création d'un compte patient

#### 2.1.2 **Voir un compte patient**

Si vous avez déjà créer des patients sur votre compte, ceux-ci s'affiche dans la liste des patients.

Vous avez la possibilité de cliquer sur le bouton *Voir* à droite du patient pour voir plus d'informations sur le patient et créer ou modifier des listes de travail pour le patient. (Après avoir cliqué sur le bouton *Voir* vous pouvez voir la section 3.)

### 2.2 **Dictionnaire**

La seconde section de ce panel est le dictionnaire. Celui représente les mots pouvant être travaillés sur l'application Artiphonie.

#### 2.2.1 **Ajouter un mot dans le dictionnaire**

Le bouton *Ajouter un mot dans le dictionnaire* vous permet d'ajouter un mot qui n'est pas présent dans le dictionnaire actuel.

Pour ajouter un mot vous devez compléter les champs demandés. Ce formulaire contient une subtilité. Pour rentrer l'écriture phonétique du mot, vous avez à disposition un clavier phonétique vous permettant d'ajouter des caractères spéciaux. Il vous suffit de cliquer sur le carré de couleur pour ajouter le symbole désiré.

Vous avez également la possibilité d'utiliser votre clavier pour compléter l'écriture phonétique. Pour utiliser l'écriture phonétique avec votre clavier physique vous devez utiliser le symbole situé entre parenthèses.

*Exemple :* pour ajouter le symbole **ɛ̃** vous pouvez appuyer sur la touche **5** de votre clavier

Cliquez ensuite sur *Ajouter mot au dictionnaire* pour ajouter le mot.

![Créer un mot pour le dictionnaire](IMG_PANEL/creer_mot_dictionnaire.png)

Image exemple pour ajouter un mot dans le dictionnaire

#### 2.2.2 **Modifier un mot**

Si un mot présente une erreur, vous pouvez le modifier en cliquant sur le bouton *Modifier* associé au mot.

Un formulaire similaire à l'ajout d'un mot s'affiche à vous. Il ne vous reste plus qu'à modifier ce que vous souhaitez.

N'oubliez pas de cliquer sur le bouton *Sauvegarder les modifications* pour que les modifications soient prises en compte.

#### 2.2.3 **Supprimer un mot**

Si vous souhaitez supprimer un mot dans le dictionnaire, il vous suffit de cliquer sur le bouton *Supprimer*. Une boite de dialogue s'ouvre, vous demandant de confirmer votre choix.

### 2.3 **Barre de navigation**

En haut à droite du panel d'Artiphonie vous avez un menu déroulant.

En cliquant sur *Mon Compte* vous avez accès à :

1. Panel - Accéder au panel de base
2. Paramètres - Accéder aux paramètres de votre compte et les modifier (voir 4.)
3. Administration (Uniquement si votre compte est promu) - Pouvoir créer et gérer des nouveaux comptes orthophonistes (voir 5.)
4. Me déconnecter - Pour vous déconnecter du panel d'Administration

## 3. **Compte Patient**

Après avoir cliqué sur *Voir* du compte patient vous avez accès à un nouveau panel.

Ici vous avez toutes les informations du compte patient (nom, prenom, age...), ainsi que les identifiants de connexion du patient sur l'application Artiphonie.

![Image compte patient](IMG_PANEL/patient.png)

Image exemple compte patient

### 3.1 **Présenation de l'interface**

Sur cette page, vous avez accès à toutes les informations liés au compte du patient.

#### 3.1.1 **Informations**

Vous avez en haut de la page, le nom, prénom, age, niveau, nombre d'étoiles gagnés et description du patient.

#### 3.1.2 **Identifiants**

Vous avez ensuite les identifiants du patient sur l'application.

Vous pouvez également supprimer le compte du patient en cliquant sur *Supprimer le patient*.

#### 3.1.2 **Filtres/Listes de travail**

Vous pouvez *créer un filtre de travail* pour le patient, *modifier la liste de travail actuelle* ou alors *retirer la liste de travail* du patient.

Les différents filtres/listes de travail de votre compte sont disponible. Il vous suffit de cliquer sur *Voir* pour afficher les mots présents dans la liste ou de cliquer sur *Sélectionner* pour faire travailler l'enfant sur cette liste.

### 3.2 **Créer une nouvelle liste de travail**

Cliquez sur le bouton *Créer une nouvelle liste de travail* associé au compte du patient.

Vous arrivez sur une nouvelle interface.

![Créer liste de travail](IMG_PANEL/nouvelle_liste.png)

Image exemple de l'interface de création de liste de travail

#### 3.2.1 **Utiliser les filtres**

Vous avez dans la partie bleue de l'interface des filtres à votre disposition pour vous aider à sélectionner les mots ou type de mots que vous souhaitez faire travailler.

#### 3.2.2 **Voir les résultats**

La partie de droite vous affiche tous les résultats par rapport aux filtres que vous avez indiqués. Vous avez également la possibilité d'ajouter un mot et de rechercher dans le dictionnaire.

#### 3.2.3 **Sélectionner un mot**

Si vous souhaitez faire travailler un mot à un patient il vous suffit de cliquer sur le bouton *Sélectionner* associé au mot. Le mot sélectionné est ensuite ajouter à votre liste de travail.

`Si vous souhaitez ajouter tous les résultats d'un coup, vous pouvez cliquez sur le bouton `*Ajouter tous les résultats*

#### 3.2.4 **Liste de travail**

Sous les résultats vous avez un apperçu de tous les mots présents dans votre liste de travail. Vous pouvez retirer un mot dans votre liste en cliquant sur le bouton *Retirer* associé au mot. Vous pouvez également vider votre liste de travail en cliquant sur *Tout retirer*.

`Il est recommandé d'avoir plus de 10 mots dans votre liste pour que les mots ne se répète pas dans l'application`

#### 3.2.5 **Créer la liste de travail**

Une fois votre sélection de mot effectué. Il vous suffit de cliquer sur le bouton *Créer la liste de travail*. Une boite de dialogue vous demande alors de rentrer un nom pour la liste. Cliquez ensuite sur *Valider* pour enregistrer et associé la liste au patient.

### 3.3 **Modifier une liste de travail**

Pour modifier une liste de travail vous devez cliquer sur le bouton *Voir* associé à la liste que vous souhaitez modifier sur l'interface d'un compte patient.

Vous arrivez ensuite sur la même interface que la création d'une liste de travail. Cependant vous retrouvez tous les mots associés à cette liste directement présent dans la liste de travail.

Vous pouvez ajouter/retirer des mots, pour valider vos modifications il vous suffit de cliquer sur *Enregistrer les modifications*.

### 3.4 **Supprimer une liste de travail**

Si vous ne voulez plus utiliser une liste de travail, il vous suffit de cliquer sur le bouton *Voir* associé à la liste sur l'interface d'un compte patient.

Puis il vous suffit de cliquer sur le bouton *Supprimer le filte* et de confirmer votre choix.


## 4. **Modifier les informations du compte**

Dans la barre de navigation, cliquez sur *Mon Compte* puis *Paramètres*.

![Image paramètre compte](IMG_PANEL/mon_compte.png)

Image exemple paramètre compte

### 4.1 **Modifier informations**

Pour modifier vos informations il vous suffit de modifier le formulaire dans la section *Mes informations* puis de cliquer sur *Enregistrer*.

### 4.2 **Changer le mot de passe**

Vous pouvez modifier votre mot de passe dans la section *Changer le mot de passe*, n'oubliez pas d'enregistrer pour que vos modifications soient prises en compte

## 5. **Administration**

Si votre compte est promu, vous avez accès à cette page vous permettant de voir tous les comptes orthophonistes disponibles sur la plateforme.

![Image management](IMG_PANEL/ortho_manage.png)

Image exemple interface management

### 5.1 **Créer un nouveau compte orthophoniste**

Pour ajouter un nouveau compte orthophoniste il vous suffit de cliquer sur le bouton *Créer un compte ortho* puis remplissez le formulaire associé. Enfin cliquez sur le bouton *Créer* pour ajouter le compte.

### 5.2 **Promouvoir un compte**

Promouvoir un compte permet de donner l'autorisation au compte concerné de créer également des nouveaux comptes.

Pour promouvoir un compte. Cliquez sur le bouton *Promouvoir* associé au compte.

### 5.3 **Rétrograder un compte**

Rétrograder un compte permet de retirer au compte la possibilité de créer des nouveaux comptes.

Pour rétrograder un compte. Cliquez sur le bouton *Rétrograder* associé au compte.

### 5.4 **Supprimer un compte**

Vous pouvez supprimer un compte orthophoniste en cliquant sur le bouton *Supprimer* associé au compte.

La suppression d'un compte va supprimer toutes les données associés (compte patient et filtres).

## 6. **Aide**

Pour obtenir de l'aide, faire des remarques ou avoir plus de renseignement n'hésitez pas à compter etienne.reygner@etu.univ-grenoble-alpes.fr