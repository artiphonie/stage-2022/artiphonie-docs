# Avancement Stage

### du 02/05/2022 au 06/05/2022

- Prise en main du projet
- Gestion des patients avec la base de données
- Gestion des orthophonies avec la base de données
- Gestion des mots avec la base de données
- Connexion au panel avec un compte orthophoniste
- Sauvegarde des données du jeu dans la bdd (niveaux débloqués, nombre d'étoile, informations du personnage, ...)
- Ajout des mots dans le dictionnaire
- Début de la création de liste de mots sur le panel d'administration

### du 09/05/2022 au 13/05/2022

- Multiuser du jeu
- Hash mot de passe ortho
- Unique email, login, mot - ortho, enfant, mot
- Implementation mots dictionnaire en jeu
- Récupération du dictionnaire en local
- Vérification si changement dans le dictionnaire
- Création, modification filtre simple (nombre de syllabe) panel administration
- Implémentation filtre dans l'application

### du 16/05/2022 au 20/05/2022

- Préparation exportation application (apk)
- Réunion avec l'orthophoniste
- Revu de la base de mot
- Application de nouveaux filtres pour créer liste de mots
- Récupération nouveaux filtres dans l'application
- Implémentation clavier phonétique pour l'orthophoniste
- Récupération des anciens jeux dans le git

### du 23/05/2022 au 25/05/2022

- Mise en place d'un site provisoire pour permettre à l'orthophiniste d'ajouter des mots dans la banque
- Préparation et exportation de l'application sur la tablette

### du 30/05/2022 au 02/06/2022

- Réglage d'exportation de l'application
- Réglage du textToSpeech, ainsi que problème de connexion à la base de données
- Exportation de l'application sur la tablette

### du 06/06/2022 au 10/06/2022

- Réglage des différents bugs sur l'application
- Début transcription mots et phonétique sur la base de données

### du 13/06/2022 au 17/06/2022

- Transcription mots
- Mise en production du première version
- Résolution de bugs

### du 20/06/2022 au 26/06/2022

- Correction de problème
- Ajout des résultats sur l'application
- Affichage des résultats sur le panel
- Liaison entraînement avec liste de travail

### du 27/06/2022 au 01/07/2022

- Rendez-vous avec l'orthophoniste
- Correction de problème
- Ajout phonem dans les filtres
- Implementation algorithme Levenshtein dans l'application

### du 04/07/2022 au 08/07/2022

- Ajout de nouvelles images
- Correction de problèmes remontés par l'orthophoniste (panel et application)
- Mise en place de la version 0.3

### du 11/07/2022 au 13/07/2022

- Version 0.3.1 mit en place
- Correction de problèmes du panel d'administration

### du 18/07/2022 au 22/07/2022

- Exportation de l'application sur le play store
- Réunion avec l'orthophoniste
- Importation de nouvelles images / vidéos
- Réglage de soucis